<!DOCTYPE HTML>
<html>
	<head>
		<title>Solid Data Workers</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">
		<section id="sidebar">
			<div class="inner">
				<nav>
					<ul>
						<li><a href="#intro">Welcome</a></li>
						<li><a href="#description">Intro</a></li>
						<li><a href="#components">Components</a></li>
					</ul>
				</nav>
			</div>
		</section>

		<div id="wrapper">
			<section id="intro" class="wrapper style1 fullscreen fade-up">
				<div class="inner">
					<h1>Solid Data Workers</h1>
					<p>Fetch real-world informations into <a href="https://solid.mit.edu/">Solid</a> to leverage your personal linked data graph.</p>
					<ul class="actions">
						<li><a href="#one" class="button scrolly">Learn more</a></li>
					</ul>
				</div>
			</section>

			<section id="description" class="wrapper style2 spotlights">
				<section>
					<div class="content">
						<div class="inner">
							<h2>Intro</h2>
							<p>With Solid you can keep your data into a personal "pod", organized into a <a href="https://www.w3.org/standards/semanticweb/data">linked data graph</a>.</p>
							<p>Which data? Solid Data Workers provide to gather informations from existing sources, elaborate and relate them, and populate the graph to provide the basis for new, innovative, integrated applications.</p>
							<img class="fullimage" src="images/intro.png">
						</div>
					</div>
				</section>
			</section>

			<section id="components" class="wrapper style3 fade-up">
				<div class="inner">
					<h2>Components</h2>
					<p>Here a collection of software components which simplify interaction with Solid, to help you building applications and Data Workers, and a collection of actual Data Workers ready to be attached to your Solid pod to fetch informations and enrich your personal linked data graph.</p>
					<div class="features">
						<section>
							<span class="icon solid major fa-code"></span>
							<h3>laravel-sparql</h3>
							<p>A PHP ORM to read and write informations through a SPARQL endpoint.</p>
							<ul class="actions">
								<li><a href="https://gitlab.com/solid-data-workers/laravel-sparql" class="button">Learn more</a></li>
							</ul>
						</section>
						<section>
							<span class="icon solid major fa-code"></span>
							<h3>laravel-solid-bridge</h3>
							<p>Laravel integration with Solid. Build your PHP application right now!</p>
							<ul class="actions">
								<li><a href="https://gitlab.com/solid-data-workers/laravel-solid-bridge" class="button">Learn more</a></li>
							</ul>
						</section>
					</div>
					<div class="features">
						<section>
							<span class="icon solid major fa-cog"></span>
							<h3>CalDAV Data Worker</h3>
							<p>Push events and meetings into Solid.</p>
							<ul class="actions">
								<li><a href="#" class="button">TBD</a></li>
							</ul>
						</section>
						<section>
							<span class="icon solid major fa-cog"></span>
							<h3>EMail Data Worker</h3>
							<p>Fetch emails and contacts into Solid.</p>
							<ul class="actions">
								<li><a href="#" class="button">TBD</a></li>
							</ul>
						</section>
					</div>
				</div>
			</section>
		</div>

		<footer id="footer" class="wrapper style1-alt">
			<div class="inner">
				<ul class="menu">
					<li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					<li>Supported By: <a href="https://nlnet.nl/"><img src="images/nlnet.png"></a></li>
				</ul>
			</div>
		</footer>

		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.scrollex.min.js"></script>
		<script src="assets/js/jquery.scrolly.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
	</body>
</html>
